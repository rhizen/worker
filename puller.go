package worker

import (
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/pkg/errors"
	"pkg.cocoad.mobi/x/http"
	"pkg.cocoad.mobi/x/zlog"
)

var DataPath = "./data"

type Result struct {
	Data []byte
	Err  error
}
type PullFunc func() <-chan Result

type Puller interface {
	Pull(c *Config) PullFunc
}

type Parser interface {
	Parse(s []byte) (<-chan interface{}, error)
}

type Transformer interface {
	Transform(item interface{}) Offer
}

func SaveToDisk(account string, now time.Time, data []byte) (string, error) {
	var mx sync.Mutex
	path := now.Format("20060102")
	filename := now.Format("150405") + ".json"
	filePath := filepath.Join(DataPath, account, path, filename)
	mx.Lock()
	defer mx.Unlock()
	if err := os.MkdirAll(filepath.Join(DataPath, account, path), 0755); err != nil {
		return "", errors.Wrapf(err, "save account %s mkdir %s failed", account, filePath)
	}
	fp, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		return "", errors.Wrapf(err, "save account %s open file %s failed", account, filePath)
	}
	defer fp.Close()
	if _, err = fp.Write(data); err != nil {
		return "", errors.Wrapf(err, "save account %s write to path %s failed", account, filePath)
	}
	return filePath, nil
}

type Config struct {
	Token     string
	Key       string
	URL       string
	PageLimit int
	Generate  func() string
}

func NewConfig(token, key, url string, limit int) *Config {
	return &Config{
		Token:     token,
		Key:       key,
		URL:       url,
		PageLimit: limit,
		Generate:  func() string { return url },
	}
}

func DefaultPuller(c *Config) PullFunc {
	return func() <-chan Result {
		resChan := make(chan Result, 0)
		go func() {
			tp := xhttp.NewTrasport()
			tp.Deadline = func() time.Time {
				return time.Now().Add(300 * time.Second)
			}
			client := xhttp.NewClient(tp)
			client.Timeout = 300 * time.Second
			data, err := FetchDataFromUrl(client, c.GenerateUrl())
			if err != nil {
				log.Error("default puller fetch data from url", log.F{"error": err})
				return
			}
			resChan <- Result{
				Data: data,
				Err:  err,
			}
			close(resChan)
		}()

		return resChan
	}
}

func (c *Config) GenerateUrl() string {
	return c.Generate()
}

func FetchDataFromUrl(client *http.Client, uri string) ([]byte, error) {
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return []byte(""), err
	}
	req.Header.Set("User-Agent", "CocoAd Affiliate Offer Collector/1.1")
	resp, err := client.Do(req)
	if err != nil {
		return []byte(""), err
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte(""), err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return []byte(""), errors.Errorf("status code not 200 %d", resp.StatusCode)
	}

	return data, nil
}
