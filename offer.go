package worker

import (
	"time"

	"github.com/shopspring/decimal"
)

type Offer struct {
	Name        string
	AccountId   int
	Platform    string
	ClickUrl    string
	TrafficType string
	BidType     string
	OsMinVer    string
	OsMaxVer    string
	Kpi         string
	ExternalID  string
	SourceID    string
	Creatives   []Creative
	Target      Target
	LaunchedAt  time.Time
	ExpiredAt   time.Time
	CreatedAt   time.Time
	UpdatedAt   time.Time
	App         Application
}

type Application struct {
	Name        string
	OsFamily    string
	IconUrl     string
	PreviewUrl  string
	PkgName     string
	PkgSize     string
	OsMinVer    string
	OsMaxVer    string
	Category    string
	Description string
	MinInstall  string
	MaxInstall  string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type Creative struct {
	Name      string
	Type      string
	Width     int
	Height    int
	Ratio     string
	Size      string
	Duration  int
	Url       string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Target struct {
	CountrySet  string
	Currency    string
	Price       decimal.Decimal
	DailyCap    int
	DailyBudget decimal.Decimal
	Status      string
	Countries   []string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
