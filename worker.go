// 调用不同的微服务(解析各个广告主返回的消息)
package worker

import (
	"crypto/rand"
	"plugin"
	"strings"
	"sync"
	"unicode"

	"pkg.cocoad.mobi/x/zlog"
)

type Worker interface {
	Puller
	Parser
	Transformer
}

var (
	medias = make(map[string]Worker, 0)
	mu     sync.Mutex
)

func LoadPlugin(name string) Worker {
	mu.Lock()
	defer mu.Unlock()
	name = strings.ToLower(name)
	p, ok := medias[name]
	if ok {
		return p
	}
	plug, err := plugin.Open("./libs/" + name + ".so")
	if err != nil {
		log.Error("load "+name+" plugin failed", log.F{"error": err})
		return nil
	}
	symbol, err := plug.Lookup(UcFirst(name))
	if err != nil {
		log.Error("lookup "+name+" Download failed", log.F{"error": err})
		return nil
	}

	if medias[name], ok = symbol.(Worker); !ok {
		log.Error("interface worker check", log.F{"name": name, "status": ok})
		return nil
	}

	return medias[name]
}

func UcFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

func UUID(length int) string {
	if length == 0 {
		return ""
	}
	chars := []byte("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	clen := len(chars)
	maxrb := 255 - (256 % clen)
	b := make([]byte, length)
	r := make([]byte, length+(length/4)) // storage for random bytes.
	i := 0
	for {
		rand.Read(r)
		for _, rb := range r {
			c := int(rb)
			if c > maxrb {
				continue // Skip this number to avoid modulo bias.
			}
			b[i] = chars[c%clen]
			i++
			if i == length {
				return string(b)
			}
		}
	}
}
